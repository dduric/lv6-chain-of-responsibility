﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_lanac
{
    class StringLowerCaseChecker : StringChecker
    {
         protected override bool PerformCheck(string stringToCheck)
        {
            if (stringToCheck.Any(c => char.IsLower(c)))
            {
                return true;
            }
            else return false;
        }
    }
}
