﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_lanac
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);

            logger.Log("Demo dummy demonstration", MessageType.INFO);

            StringChecker stringChecker = new StringDigitChecker();
            StringChecker stringChecker2 = new StringLowerCaseChecker();
            StringChecker stringChecker3 = new StringUpperCaseChecker();

            StringDigitChecker stringDigitChecker = new StringDigitChecker();
            StringLowerCaseChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpperCaseChecker = new StringUpperCaseChecker();

            stringChecker.SetNext(stringDigitChecker);
            stringChecker.Check("asdasdDA23ads");
            stringChecker.Check("asdsadasd");

            stringChecker2.SetNext(stringLowerCaseChecker);
            stringChecker2.Check("asdasdDA23ads");
            stringChecker2.Check("ASJLHLJASHLF");

            stringChecker3.SetNext(stringUpperCaseChecker);
            stringChecker3.Check("asdasdDA23ads");
            stringChecker3.Check("asdsadasd");

        }
    }
}
