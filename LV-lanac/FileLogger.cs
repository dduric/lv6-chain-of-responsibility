﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_lanac
{
    class FileLogger : AbstractLogger
    {
        private string filePath;
        public FileLogger(MessageType messageType, string filePath) : base(messageType)
        {
            this.filePath = filePath;
        }
        protected override void WriteMessage(string message, MessageType type)
        {
            using (System.IO.StreamWriter writer
                = new System.IO.StreamWriter(filePath, true))
            {
                writer.WriteLine(DateTime.Now.ToLongTimeString());
                writer.WriteLine(message);
            }
        }
    }
}
