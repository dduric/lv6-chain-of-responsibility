﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV_lanac
{
    enum MessageType
    {
        INFO = 1, // 1
        WARNING = 2, // 10
        ERROR = 4, //100
        ALL = 7 //111
    }
}
